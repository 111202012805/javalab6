package mahasiswa;
import java.util.Date;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class Mahasiswa {
	public String nim;
	String nama;
	Double ipk;
	int sks;
	String tglLahir; //formatnya yyyy-mm-dd
	Calendar kld = Calendar.getInstance();
	
	public Mahasiswa(String nim, String nama, Double ipk, int sks, String tglLahir) {
		this.nim = nim;
		this.nama = nama;
		this.ipk = ipk;
		this.sks = sks;
		this.tglLahir = tglLahir;
	}
	
	public String getProgdi() {
		String progdi = this.nim.substring(0, 3);
		String ps="";
		if(progdi.equals("A11")) {
			ps = "Teknik Informatika";
		}
		else if(progdi.equals("A12")) {
			ps = "Sistem Informasi";
		}
		else if(progdi.equals("B11")) {
			ps = "Manajemen";
		}
		else if(progdi.equals("B12")) {
			ps = "Akuntansi";
		}
		else {
			ps = "Belum terdaftar";
		}
		
		return ps;
	}
	
	public String ipkStatus() {
		String stat="";
		if(this.ipk <= 1 && this.ipk > 0) {
			stat = "Perlu usaha lebih";
		} else if (this.ipk <= 2) {
			stat = "Perlu segera menaikkan IPK";
		} else if (this.ipk <= 2.75) {
			stat = "Sedikit lagi";
		} else if (this.ipk <= 3) {
			stat = "Memuaskan";
		} else if (this.ipk <= 3.5) {
			stat = "Sangat Memuaskan";
		} else if (this.ipk <= 4) {
			stat = "Dengan pujian";
		} else {
			stat = "Range ipk di luar jalur";
		}
		
		return stat;
	}
	
	public int getTahun() {
		int angkatan = Integer.parseInt(this.nim.substring(4, 8));
		return angkatan;
	}
	
	public String getTagihanSks() {
		int perSks = 250000;
		int tagihan = this.sks * perSks;
		
		return currencyFormatter(tagihan);
	}
	
	public int getMhsSemester() {
		int smt = 0;
		int lamaKuliah;
		int thnSkr = kld.get(Calendar.YEAR);
		int blnSkr = kld.get(Calendar.MONTH);
		lamaKuliah = thnSkr - getTahun();
		smt = lamaKuliah * 2;
		if (blnSkr <= 6) {
			smt -= 1;
		}
		
		return smt;
	}
	
	public int getUmur() {
		int umur;
		int thnSkr = kld.get(Calendar.YEAR);
		int blnSkr = kld.get(Calendar.MONTH);
		int tglSkr = kld.get(Calendar.DATE);
		int thnLhr = Integer.parseInt(this.tglLahir.substring(0, 4));
		int blnLhr = Integer.parseInt(this.tglLahir.substring(5, 7));
		int tglLhr = Integer.parseInt(this.tglLahir.substring(8));
		
		umur = thnSkr - thnLhr;
		if (blnSkr <= blnLhr) {
			if (tglSkr < tglLhr) {
				umur-=1;
			}
		}
		
		return umur;
	}
	
	private Date dateFormatter(String pola, String tanggal) {
		Date tgl=null;
		SimpleDateFormat formatter = new SimpleDateFormat(pola);
		
		try {
			tgl = formatter.parse(tanggal);
		}
		catch(ParseException e) {
			e.printStackTrace();
		}
		return tgl;
	}
	
	private String currencyFormatter(int currency) {
		String pattern = "###,###.00";
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator(',');
		symbols.setGroupingSeparator('.');
		DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
		
		return decimalFormat.format(currency);
	}
}